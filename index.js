import { AppRegistry } from 'react-native';
import App from './src/App';
import AmbulatorioNav from './src/components/navigations/AmbulatorioNav';
import Aplicacoes from './src/components/mapInfo/Aplicacoes';
import CustomDrawerContentComponent from './src/components/common/customSide';

import { DrawerNavigator } from 'react-navigation';

const Drawer = DrawerNavigator({
  App: { screen: App },
  Cities: { screen: AmbulatorioNav },
}, {
  contentComponent: CustomDrawerContentComponent
});

AppRegistry.registerComponent('Alagoas', () => Drawer);
