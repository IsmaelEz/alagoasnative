import React from 'react';
import {
  StyleSheet,
  View,
  Dimensions
} from 'react-native';

import MapView, { Marker } from 'react-native-maps';

import firebase from './config/firebase';
import MainMap from './components/map';
import ListItem from './components/ListItem';
import Header from './components/header';
import Loading from './components/common/loading';

const database = firebase.database();

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = -9.5713058;
const LONGITUDE = -36.7819505;
const LATITUDE_DELTA = 10.0;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  }
});

class App extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Principal',
    title: 'Principal'
  };

  constructor(props) {
    super(props);

    this.state = {
      datas: [],
      loading: true,
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }
    };
  }

  componentWillMount() {
    const states = database.ref('/datas/').on('value', (snap) => {
      var finished = [];
      snap.forEach((data) => {
          let result = data.val();
          result["key"] = data.key;
          finished.push(result);
      });
      this.setState({
        datas: finished,
        loading: false
      });
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={ '+Alagoas' }
          onPress={ () => this.props.navigation.navigate('DrawerOpen') }
        />

        {this.state.loading ?
          <Loading /> :
          <MainMap
            datas={ this.state.datas }
            initialRegion={ this.state.region }
          />
        }
      </View>
    );
  }
}

export default App;
