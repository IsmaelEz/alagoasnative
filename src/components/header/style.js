import React from 'react';
import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  headerStyle: {
    backgroundColor: '#FFF',
    marginTop: 0,
    paddingLeft: 20,
    paddingRight: 20,
    height: 60,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    elevation: 5
  },
  headerText: {
    fontSize: 20,
    fontWeight: '300',
    width: '100%',
    textAlign: 'center',
    fontWeight: 'bold'
  },
  headerIcon: {
    position: 'absolute',
    padding: 10
  }
});

export default style;
