import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableHighlight
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import style from './style';

const Header = (props) => {
  const {
    headerStyle,
    headerText,
    headerIcon
  } = style;

  return (
    <View style={ headerStyle }>
      <StatusBar
        backgroundColor={ 'rgba(0,0,0,.1)' }
        barStyle= 'dark-content'
        hidden={ false }
      />

      <TouchableHighlight
        style={ headerIcon }
        underlayColor={ 'rgba(0,0,0,.05)' }
        onPress={ props.onPress }>

        <Text><Icon name="menu" size={30} color={'#afafaf'} /></Text>
      </TouchableHighlight>

      <Text style={ headerText }>{ props.title }</Text>
    </View>
  );
}

export default Header;
