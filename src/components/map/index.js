import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet
} from 'react-native';

import MapView, { Marker } from 'react-native-maps';

import styles from './style';

let coordinateSaved = []

const MainMap = (props) => {
  const { initialRegion, datas } = props;

  datas.map(data => {
    if (data.coordinate !== undefined) {
      coordinateSaved.push(data);
    }
  });

  return (
    <View style ={styles.container}>
      <MapView
        loadingEnabled={true}
        style={styles.map}
        zoomEnabled={true}
        zoomControlEnabled={true}
        minZoomLevel={7.5}
        maxZoomLevel={20}
        initialRegion={ initialRegion }
      >
      {coordinateSaved.map(data => (
        <Marker
          key={ data.key }
          title={ data.setor }
          coordinate={ data.coordinate }
          pinColor={'red'}
        />
      ))}
      </MapView>
    </View>
  );
};

export default MainMap;
