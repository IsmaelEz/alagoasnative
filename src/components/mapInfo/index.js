import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList
} from 'react-native';

import firebase from '../../config/firebase';

import styles from './style';
import Header from '../header';
import Loading from '../common/loading';
import ListCity from './ListCity';
import CityView from './CityView';

const database = firebase.database();


class Cities extends React.Component {
  static navigationOptions = {
    drawerLabel: 'Ambulatórios',
    headerTitle: <Header title={'Lista dos Ambulatórios'}
                  onPress={ () => this.props.navigation.navigate('DrawerOpen') } />
  }

  constructor(props) {
    super(props);
    this.state = {
      datas: [],
      loading: true,
    }
  }

  componentWillMount() {
    const states = database.ref('/datas/').on('value', (snap) => {
      var finished = [];
      snap.forEach((data) => {
          let result = data.val();
          result["key"] = data.key;
          finished.push(result);
      });
      this.setState({
        datas: finished,
        loading: false
      });
    });
  }

  render() {
    return (
      <View style={styles.container}>
          {this.state.loading ?
            <Loading /> :
            <ListCity
              datas={ this.state.datas }
              navigation={ this.props.navigation }
              />
          }
      </View>
    );
  }
}

export default Cities;
