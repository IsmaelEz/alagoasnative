import React from 'react';
import {
  ScrollView,
  Text
} from 'react-native';

import Header from '../header';
import Loading from '../common/loading';
import firebase from '../../config/firebase';

const database = firebase.database();

class Comparativos extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      datas: [],
      loading: true
    };
  }

  static navigationOptions = {
    drawerLabel: 'Comparativos'
  }

  componentWillMount() {
    const states = database.ref('/datas/').on('value', (snap) => {
      var finished = [];
      snap.forEach((data) => {
          let result = data.val();
          result["key"] = data.key;
          finished.push(result);
      });
      this.setState({
        datas: finished,
        loading: false
      });
    });
  }

  render() {
    return (
      <ScrollView>
        <Header
          title={'Comparativos'}
          onPress={ () => this.props.navigation.navigate('DrawerOpen') }
          />

          {this.state.loading ?
            <Loading /> :
            <Text>LoadedRalho</Text>
          }
      </ScrollView>
    );
  }
};

export default Comparativos;
