import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF'
  },
  itemContainer: {
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    borderBottomColor: '#f2f4f7',
    borderBottomWidth: 2
  },
  itemText: {
    fontWeight: 'bold',
    fontSize: 15
  }
});

export default styles;
