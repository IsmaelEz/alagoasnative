import React from 'react';
import {
  ScrollView,
  Text
} from 'react-native';


class Produtos extends React.Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    title: 'Lista dos Produtos'
  }

  render() {
    return (
      <ScrollView>
        <Text>{ this.props.navigation.getParam('title') }</Text>
        <Text>{ this.props.navigation.getParam('itemName') }</Text>
      </ScrollView>
    );
  }
}

export default Produtos;
