import React from 'react';
import {
  ScrollView,
  View,
  Text
} from 'react-native';

import firebase from '../../config/firebase';
import { BarChart, XAxis, YAxis, Grid } from 'react-native-svg-charts';
import Loading from '../common/loading';

const database = firebase.database();


class Aplicacoes extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      datas: [],
      loading: true,
    }
  }

  static navigationOptions = {
    title: 'Aplicações'
  }

  componentWillMount() {
    const states = database.ref('/datas/').on('value', (snap) => {
      var finished = [];
      snap.forEach((data) => {
          let result = data.val();
          result["key"] = data.key;
          finished.push(result);
      });
      this.setState({
        datas: finished,
        loading: false
      });
    });
  }

  render() {
    let productValue = [];
    const fill = 'rgb(134, 65, 244)';

    const dataFiltered = this.state.datas.filter((data,index,self) => {
      return index === self.indexOf(data);
    });

    dataFiltered.map(value => {
      productValue.push(value.valor_unitario);
    })

    return (
      <ScrollView>
        {this.state.loading ?
          <Loading /> :
          <View style={{ height: 200, padding: 20 }}>
            <BarChart
              style={{ flex: 1 }}
              data={ productValue }
              contentInset={{ top: 10, bottom: 10 }}
              svg={{ fill }}
            >
                <Grid/>
            </BarChart>
            <XAxis
              style={{ marginHorizontal: -10 }}
              data={ productValue }
              formatLabel={ value => value }
              contentInset={{ left: 10, right: 10 }}
              svg={{ fontSize: 10, fill: 'black' }}
            />
            <YAxis
              data={ productValue }
              numberOfTicks={ 10 }
              formatLabel={ value => value }
              contentInset={{ left: 10, right: 10 }}
              svg={{ fontSize: 10, fill: 'black' }}
            />
          </View>
        }
      </ScrollView>
    );
  }
}

export default Aplicacoes;
