import React from 'react';
import {
  FlatList,
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import styles from './style';


class ListCity extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { datas, navigation } = this.props;

    let coordinateSaved = [];

    datas.map(data => {
      if (data.coordinate !== undefined) {
        coordinateSaved.push(data);
      }
    });

    return (
      <View>
        <FlatList
          data={ coordinateSaved }
          renderItem={({item}) =>
            <TouchableOpacity onPress={ () => navigation.navigate('AmbulatorioView', {
              itemName: item.setor,
              title: item.setor
            })} >
             <View style={ styles.itemContainer }>
               <Text style={ styles.itemText }>
                 { item.setor }
               </Text>
             </View>
           </TouchableOpacity>
          }
        />
      </View>
    )
  }
};

export default ListCity;
