import React from 'react';
import {
  StyleSheet,
  ScrollView,
  Text,
  FlatList
} from 'react-native';

import firebase from '../../config/firebase';

import styles from './style';

const database = firebase.database();

class CityView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      datas: [],
      loading: true,
    }
  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;

    return {
      title: params ? params.title : ''
    }
  }

  componentWillMount() {
    const states = database.ref('/datas/').on('value', (snap) => {
      var finished = [];
      snap.forEach((data) => {
          let result = data.val();
          result["key"] = data.key;
          finished.push(result);
      });
      this.setState({
        datas: finished,
        loading: false
      });
    });
  }

  render() {
    let itemData = [];
    const itemName = this.props.navigation.getParam('itemName');

    const dataFiltered = this.state.datas.filter((data) => {
      return data.setor === itemName;
    })

    return (
      <ScrollView style={styles.container}>
        {dataFiltered.map(data => (
          <Text>{ data.setor }</Text>
        ))}
      </ScrollView>
    );
  }
}

export default CityView;
