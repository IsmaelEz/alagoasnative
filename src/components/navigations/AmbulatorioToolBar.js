import React from 'react';
import { TabNavigator } from 'react-navigation';

import Produtos from '../mapInfo/Produtos';


const AmbulatorioToolBar = TabNavigator({
  Medicamentos: { screen: Produtos }
}, {
  initialRouteName: 'Medicamentos',
  navigationOptions: {
    tabBarVisible: false
  }
});

export default AmbulatorioToolBar;
