import React from 'react';
import { StackNavigator } from 'react-navigation';

import Cities from '../mapInfo';
import AmbulatorioToolBar from './AmbulatorioToolBar';


const AmbulatorioNav = StackNavigator({
  Ambulatorio: { screen: Cities },
  AmbulatorioView: { screen: AmbulatorioToolBar }
}, {
  initialRouteName: 'Ambulatorio',
});

export default AmbulatorioNav;
