import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  ActivityIndicator
} from 'react-native';

import styles from './style';

const Loading = () => {
  const { loading, textStyle } = styles;

  return (
    <View style={ loading }>
      <Text style={ textStyle }>Carregando, aguarde...</Text>
      <ActivityIndicator size='large' color='#22cccc' />
    </View>
  )
};

export default Loading;
