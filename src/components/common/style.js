import React from 'react';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: 'center'
  },
  textStyle: {
    fontSize: 18,
    textAlign: 'center',
    marginBottom: 10
  }
});

export default styles;
